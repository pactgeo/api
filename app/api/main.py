from fastapi import APIRouter

from app.api.routes import communities, debates, login, users

api_router = APIRouter()
api_router.include_router(login.router, tags=["login"])
api_router.include_router(users.router, prefix="/users", tags=["users"])
api_router.include_router(
    communities.router, prefix="/communities", tags=["communities"]
)
api_router.include_router(debates.router, prefix="/debates", tags=["debates"])
